Welcome to Discount Mini Storage of Tampa, FL! We have a variety of Climate and Non-Climate Controlled unit sizes to achieve your storage solution. Our on-site management team takes pride in personal service and maintaining an extremely clean, secure and pest free environment.

Address: 6500 N. 56th Street, Tampa, FL 33610

Phone: 813-630-9300